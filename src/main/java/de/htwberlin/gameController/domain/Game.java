package de.htwberlin.gameController.domain;

import java.util.ArrayList;
import de.htwberlin.deckController.domain.Deck;

//
/**
 * The Class Game.
 */
public class Game {
	
	private Deck deckOpen;
	private Deck deckClosed;
	private ArrayList<Player> players;
	private int whosTurn;
	private boolean mau;
	private boolean maumau;

	

	/**
	 * Instantiates a new game.
	 *
	 * @param playerList the player list
	 */
	public Game() {
		
		this.deckClosed = new Deck();
		this.deckOpen = new Deck();
		this.players = new ArrayList<Player>();
		this.whosTurn = 0;
		this.mau = false;
		this.maumau = false;
	}
	
	/**
	 * Gets the open deck.
	 *
	 * @return the deck open
	 */
	public Deck getDeckOpen() {
		return deckOpen;
	}
	
	/**
	 * Gets the closed deck.
	 *
	 * @return the deck closed
	 */
	public Deck getDeckClosed() {
		return deckClosed;
	}
	
	
	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public ArrayList<Player> getPlayers() {
		return players;
	}

	/**
	 * Gets whosTurn
	 * @return
	 */
	public int getWhosTurn() {
		return whosTurn;
	}

	/**
	 * Sets whosTurn
	 * @param whosTurn
	 */
	public void setWhosTurn(int whosTurn) {
		this.whosTurn = whosTurn;
	}
	
	public boolean isMau() {
		return mau;
	}

	public void setMau(boolean mau) {
		this.mau = mau;
	}

	public boolean isMaumau() {
		return maumau;
	}

	public void setMaumau(boolean maumau) {
		this.maumau = maumau;
	}
}
