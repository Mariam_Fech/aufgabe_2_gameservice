package de.htwberlin.gameController.domain;
import de.htwberlin.deckController.domain.Deck;
import de.htwberlin.deckController.domain.Card;
//
/**
 * The Class Player.
 */
public class Player {

	/** Players's name */
	private String nickname;
	
	/** Player's deck. */
	private Deck pDeck;
	
	/**
	 * Constructor instantiates a new player.
	 *
	 * @param nickname player name
	 */
	public Player(String nickname) {
		this.nickname = nickname;
		this.pDeck = new Deck();
	}
	
	/**
	 * Gets player's nickname.
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	
	/**
	 * Gets the player's deck.
	 * @return the players deck
	 */
	public Deck getpDeck() {
		return pDeck;
	}
	
	
	
}
