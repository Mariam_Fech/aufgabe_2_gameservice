package de.htwberlin.gameController.impl;

import java.util.ArrayList;
import de.htwberlin.deckController.domain.Card;
import de.htwberlin.deckController.domain.Deck;
import de.htwberlin.deckController.domain.Suit;
import de.htwberlin.deckController.impl.DeckService;
import de.htwberlin.gameController.domain.Game;
import de.htwberlin.gameController.domain.Player;
import de.htwberlin.gameController.inter.GameServiceInt;
import de.htwberlin.ruleController.impl.RuleService;
import de.htwberlin.viewService.impl.ViewService;

public abstract class GameService implements GameServiceInt{
	
	private RuleService rule = new RuleService();
	private DeckService deckService = new DeckService();
	private ViewService view = new ViewService();
	
	private Game game;
	
	
	@Override
	public void addPlayer(Game game, Player p) {
		this.game = game;
		game.getPlayers().add(p);	
	}

	@Override
	public void handOut(Deck deck, ArrayList<Player> players) {
		for(int i=0; i<5;i++) {
			for(Player p: players) {
				takeCard(deck,p);
			}
		}
	}

	@Override
	public void placeCard(Deck openDeck, Player p, Card myCard) {
		ArrayList<Card> cards = p.getpDeck().getCards();
		int indexCard = cards.indexOf(myCard);
		cards.remove(indexCard);
		openDeck.getCards().add(myCard);	
	}

	@Override
	public Card takeCard(Deck deck, Player p) {
		ArrayList<Card> cards = deck.getCards();
		int lastIndex = cards.size()-1;
		Card c = cards.remove(lastIndex);
		p.getpDeck().getCards().add(c);
		return c;
	}

	@Override
	public void sayMau(Player p) {
		view.outputMau(p.getNickname());	
	}

	@Override
	public void sayMauMau(Player p) {
		view.outputMauMau(p.getNickname());
	}
	
	@Override
	public void showScore(ArrayList<Player> players) {
		ArrayList<Integer> scores = new ArrayList<Integer>();
		for(Player p: players) {
			scores.add(deckService.countScore(p.getpDeck()));
		}
		view.outputScore(scores);
	}
	
	@Override
	public int nextPlayer(Game game) {
		if(game.getWhosTurn()!= (game.getPlayers().size()-1)) {
			game.setWhosTurn(game.getWhosTurn());
			return game.getWhosTurn();
		}else return 0;
	}

	@Override
	public Suit wish() {
		int i = view.outputWish();
		if (i == 1)	return Suit.hearts;	 
		if (i == 2)	return Suit.diamonds;
		if (i == 3)	return Suit.clubs;
		if (i == 4) return Suit.spades;
		else return null;
	}

	@Override
	public void initGame() {
		ArrayList<String> input = view.outputConfig();
		this.game = new Game();
		addPlayer(game, new Player(input.get(0)));
		addPlayer(game, new Player("PlayBot"));
		deckService.generateDeck(game.getDeckClosed(), Integer.parseInt(input.get(1)));
		deckService.mixCards(game.getDeckClosed());
		
		
	}
	@Override
	public void startGame() {
		initGame();
		handOut(game.getDeckClosed(), game.getPlayers());
		
		Card open = deckService.turnFirstCard(game.getDeckClosed(), game.getDeckOpen());
		view.outputOpenCard(open.getSuit().toString(), open.getRank().toString());
		
		ArrayList<String> suits = deckService.getSuitsOfDeck(game.getPlayers().get(0).getpDeck());
		ArrayList<String> ranks = deckService.getRanksOfDeck(game.getPlayers().get(0).getpDeck());
		view.outputCardsPlayer(suits, ranks);
		
		Card choice = game.getPlayers().get(0).getpDeck().getCards().get(view.outputPlaceCard()-1);
		rule.getCardEffect(open);
		rule.moveAllowed(choice, open, condition).
		
		
	}

	public RuleService getRule() {
		return rule;
	}

	public void setRule(RuleService rule) {
		this.rule = rule;
	}

	public DeckService getDeckService() {
		return deckService;
	}

	public void setDeckService(DeckService deckService) {
		this.deckService = deckService;
	}

	public ViewService getView() {
		return view;
	}

	public void setView(ViewService view) {
		this.view = view;
	}

}
