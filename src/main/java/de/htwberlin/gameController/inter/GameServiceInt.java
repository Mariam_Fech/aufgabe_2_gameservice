package de.htwberlin.gameController.inter;

import de.htwberlin.deckController.domain.Card;
import de.htwberlin.deckController.domain.Deck;
import de.htwberlin.deckController.domain.Suit;
import de.htwberlin.deckController.inter.*;

import java.util.ArrayList;
import de.htwberlin.gameController.domain.Game;
import de.htwberlin.gameController.domain.Player;


/**
 * @author Comp
 *
 */
public interface GameServiceInt extends DeckServiceInt{
	
	
	/**
	 * Set up the game with config data usename and game variety
	 */
	public void initGame();
	
	/**
	 * Add a player to the game
	 * 
	 * @param game
	 * @param p the player
	 */
	public void addPlayer(Game game, Player p);
	
	/**
	 * Start the game.
	 */
	public void startGame();
	
	/**
	 * Hands out the cards from the deck to the players.
	 *
	 * @param deck which is covered
	 * @param players the players
	 */
	public void handOut(Deck deck, ArrayList<Player> players);
	
	/**
	 * Player places a card.
	 *
	 * @param openDeck the open deck
	 * @param p the player
	 * @param myCard the my card
	 */
	public void placeCard(Deck openDeck, Player p, Card myCard);
	
	/**
	 * Player takes a card.
	 *
	 * @param deck the deck
	 * @param p the player
	 * @return card the top card from the deck
	 */
	public Card takeCard(Deck deck, Player p);
	
	/**
	 * Move on to the next player.
	 * @param game 
	 * @return int whosTurn
	 */
	public int nextPlayer(Game game);
	
	/**
	 * Player wishes a suit.
	 *
	 * @return Suit
	 */
	public Suit wish();
	/**
	 * Player says mau.
	 *
	 * @param p the player
	 */
	public void sayMau(Player p);
	
	/**
	 * Player says mau mau.
	 *
	 * @param p the player
	 */
	public void sayMauMau(Player p);
	
	/**
	 * Show the end score of the players.
	 *
	 * @param players the players
	 */
	public void showScore(ArrayList<Player> players);
	
	
	
	
}
